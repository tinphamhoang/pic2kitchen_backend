from django.http import HttpResponse
import json

def hello(request):
    if request.method =="GET":
        a = json.dumps({"key": "hello"})

        return HttpResponse(a)